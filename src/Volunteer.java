
public class Volunteer extends staffMember{

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    @Override
    public String toString() {

        String str = "ID :"+id+"\n"+"Name :"+name+"\n"+"Address :"+address+"\n";
        return str;

    }

    @Override
    public double pay() {
        return 0;
    }
}
