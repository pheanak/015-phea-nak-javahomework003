import java.util.*;

public class Main {
    //   input only numbers
    public static   int validationNumber(String st){
        Scanner input = new Scanner(System.in);
        int number=0;
        while (true){
            System.out.print(st);
            String str = input.nextLine();
            if (str.matches("[0-9][0-9]*")){
                number = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPlease Input Numbers\n");
        }
        return number;
    }

    //   input only String
    public static   String validationString(String st){
        Scanner input = new Scanner(System.in);
        String values;
        while (true){
            System.out.print(st);
            String str = input.nextLine();
            if (str.matches("[a-zA-Z]+\\.?")){
                values =str;
                break;
            }
            else
                System.out.println("\nPleas Input String And No Space\n");
        }
        return values;
    }

    public  void display( ArrayList<staffMember> list){
        Collections.sort(list, new Comparator<staffMember>() {
            @Override
            public int compare(staffMember s1, staffMember s2) {
                return (s1.getName().toUpperCase()).compareTo(s2.getName().toUpperCase());
            }
        });

        for (staffMember s : list){
            System.out.println(s);
        }

    }

    public void Edit(ArrayList<staffMember> list){
        int idEdit = validationNumber("=> Enter Staff Member's ID : ");
        for (staffMember s : list){
            if (s.getId()==idEdit){
                System.out.println(s);
                if(s instanceof Volunteer){
                    String nameVolunteer = validationString("=> Enter Staff Member's Name : ");
                    String  addressVolunteer = validationString("=> Enter Staff Member's Address : ");
                    s.setName(nameVolunteer);
                    s.setAddress(addressVolunteer);
                }
                else if(s instanceof SalariedEmployee){
                    String nameSalariedEmployee = validationString("=> Enter Staff Member's Name : ");
                    String  addressSalariedEmployee = validationString("=> Enter Staff Member's Address : ");
                    double salarySalariedEmployee = validationNumber("Enter Staff Member's Salary : ");
                    double bonusSalariedEmployee = validationNumber("Enter Staff Member's Bonus : ");
                    s.setName(nameSalariedEmployee);
                    s.setAddress(addressSalariedEmployee);
                    ((SalariedEmployee) s).setSalary(salarySalariedEmployee);
                    ((SalariedEmployee) s).setBonus(bonusSalariedEmployee);
                }
                else {
                    String nameHourlyEmployee = validationString("=> Enter Staff Member's Name : ");
                    String  addressHourlyEmployee = validationString("=> Enter Staff Member's Address : ");
                    double HourEmployee = validationNumber("Enter Staff Member's Hour : ");
                    double rateEmployee = validationNumber("Enter Staff Member's Rate : ");
                    s.setName(nameHourlyEmployee);
                    s.setAddress(addressHourlyEmployee);
                    ((HourlyEmployee) s).setHoursWorked(HourEmployee);
                    ((HourlyEmployee) s).setRate(rateEmployee);
                }
            }
        }
        display(list);
    }
    public void Remove(ArrayList<staffMember> list){
        int idRemove = validationNumber("=> Enter Staff Member's ID : ");
        Iterator<staffMember> itr = list.iterator();
        while (itr.hasNext()) {
            staffMember number = itr.next();
            if (number.getId() == idRemove) {
                itr.remove();
            }
        }
        display(list);

    }
    public  void Exit(){
        System.out.println("-> Good bye!!!");
        System.exit(0);
    }

    public static void main(String[] args) {
        staffMember volunteer = new Volunteer(1, "Narak", "BTB");
        staffMember salariedEmployee = new SalariedEmployee(2, "Rithy", "SR", 500, 150);
        staffMember hourlyEmployee = new HourlyEmployee(3, "Nary", "KPC", 8, 10);
        ArrayList<staffMember> list = new ArrayList<staffMember>();
        list.add(volunteer);
        list.add(salariedEmployee);
        list.add(hourlyEmployee);
        Main obj = new Main();
        obj.display(list);
        while (true) {
            System.out.print("1).Add Employee ");
            System.out.print("2).Edit ");
            System.out.print("3).Remove ");
            System.out.println("4).Exit ");
            int choose = validationNumber("=> Choose Option : ");
            switch (choose) {
                case 1:
                    System.out.print("1).Volunteer ");
                    System.out.print("2).Hourly Employee ");
                    System.out.print("3).Salary Employee ");
                    System.out.println("4).Back ");
                    int choose2 = validationNumber("=> Choose Option : ");
                    switch (choose2) {
                        case 1:
                            int idVolunteer = validationNumber("=> Enter Staff Member's ID : ");
                            String nameVolunteer = validationString("=> Enter Staff Member's Name : ");
                            String addressVolunteer = validationString("=> Enter Staff Member's Address : ");
                            list.add(new Volunteer(idVolunteer, nameVolunteer, addressVolunteer));
                            obj.display(list);
                            break;
                        case 2:
                            int idHourlyEmployee = validationNumber("=> Enter Staff Member's ID : ");
                            String nameHourlyEmployee = validationString("=> Enter Staff Member's Name : ");
                            String addressHourlyEmployee = validationString("=> Enter Staff Member's Address : ");
                            double HourEmployee = validationNumber("Enter Staff Member's Hour : ");
                            double rateEmployee = validationNumber("Enter Staff Member's Rate : ");
                            list.add(new SalariedEmployee(idHourlyEmployee, nameHourlyEmployee, addressHourlyEmployee
                                    , HourEmployee, rateEmployee));
                            obj.display(list);
                            break;
                        case 3:
                            int idSalariedEmployee = validationNumber("=> Enter Staff Member's ID : ");
                            String nameSalariedEmployee = validationString("=> Enter Staff Member's Name : ");
                            String addressSalariedEmployee = validationString("=> Enter Staff Member's Address : ");
                            double salarySalariedEmployee = validationNumber("Enter Staff Member's Salary : ");
                            double bonusSalariedEmployee = validationNumber("Enter Staff Member's Bonus : ");
                            list.add(new SalariedEmployee(idSalariedEmployee, nameSalariedEmployee,
                                    addressSalariedEmployee, salarySalariedEmployee, bonusSalariedEmployee));
                            obj.display(list);
                            break;
                        case 4:
                            break;
                    }
                    break;
                case 2:
                    obj.Edit(list);
                    break;
                case 3:
                    obj.Remove(list);
                    break;
                case 4:
                    obj.Exit();
                    break;
            }
        }
    }
}
